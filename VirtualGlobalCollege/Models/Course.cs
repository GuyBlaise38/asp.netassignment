﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VirtualGlobalCollege.Models
{
    public class Course
    {
        
            [DatabaseGenerated(DatabaseGeneratedOption.None)]
            public int Id { get; set; }

            [Display(Name ="Course title")]
            public string title { get; set; }

           [Display(Name = "Course credit")]
           public int credits { get; set; }

           [Display(Name = "Course cost")]
            public decimal cost { get; set; }

          

            



    }
}