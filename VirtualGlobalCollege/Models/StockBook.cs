﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VirtualGlobalCollege.Models
{
    public class StockBook
    {
        
        public int Id { get; set; }

        [Display(Name ="Book title")]
        public string title { get; set; }

        [Display(Name = "Book cost")]
        public decimal cost { get; set; }

       
        
        
    }   
}