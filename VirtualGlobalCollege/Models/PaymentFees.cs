﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualGlobalCollege.Models
{
    public class PaymentFees
    {

        public int Id { get; set; }
        public DateTime paymentDT { get; set; }
        public string paymentMethod { get; set; }


        public int enrolmentId { get; set; }
        public virtual Enrolment enrolment { get; set; }

    }
}