﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualGlobalCollege.Models
{
    public class StockBooksOnHand
    {
        public int Id { get; set; }
        public int quantity { get; set; }

        public int bookConditionId { get; set; }
        public virtual BookCondition bookCondtion { get; set; }

        public int stockbookId { get; set; }
        public virtual StockBook stockBook { get; set; }
    }
}