﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualGlobalCollege.Models
{
    public class Attendance
    {
        public int Id { get; set; }
        public Boolean status { get; set; }


        public int studentId { get; set; }
        public virtual Student student { get; set; }

        public int courseScheduleId { get; set; }
        public virtual CourseSchedule courseSchedule { get; set; }



    }
}