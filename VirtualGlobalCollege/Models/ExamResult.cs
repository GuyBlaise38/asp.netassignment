﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualGlobalCollege.Models
{
    public class ExamResult
    {
        public int Id { get; set; }
        public int examScore { get; set; }
        public DateTime examDT { get; set; }


        public int enrolmentId { get; set; }
        public virtual Enrolment enrolment { get; set; }

    }
}