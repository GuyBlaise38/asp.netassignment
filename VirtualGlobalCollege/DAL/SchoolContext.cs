﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.DAL
{
    public class SchoolContext : DbContext
    {
        public SchoolContext() : base("SchoolContext")
        {
        }

        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseSchedule> CourseSchedules { get; set; }

        public DbSet<Enrolment> Enrollments { get; set; }
        public DbSet<ExamResult> ExamResults { get; set; }
        public DbSet<PaymentFees> PaymentFees { get; set; }
        /*
        public DbSet<StockCondition> StockConditions { get; set; }
        public DbSet<StockOnHand> StockOnHands { get; set; }

        public DbSet<StockProduct> StockProducts { get; set; }
        public DbSet<StockType> Stocktypes { get; set; }
        */
        public DbSet<Student> Students { get; set; }
        

        public DbSet<StockBook> StockBooks { get; set; }
        public DbSet<BookCondition> BookConditions { get; set; }
        public DbSet<StockBooksOnHand> StockBookOnHand { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }

        }
    }