﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VirtualGlobalCollege.Startup))]
namespace VirtualGlobalCollege
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
