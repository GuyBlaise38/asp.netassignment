﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VirtualGlobalCollege.DAL;
using VirtualGlobalCollege.Models;

namespace VirtualGlobalCollege.Controllers
{
    public class StockBooksController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: StockBooks
        public ActionResult Index(string searchName)
        {
            var book = from m in db.StockBooks
                       select m;
            if (!String.IsNullOrEmpty(searchName))
            {
                book = book.Where(s => s.title.Contains(searchName));
            }


            return View(book);
        }

        // GET: StockBooks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockBook stockBook = db.StockBooks.Find(id);
            if (stockBook == null)
            {
                return HttpNotFound();
            }
            return View(stockBook);
        }

        // GET: StockBooks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StockBooks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,title,cost")] StockBook stockBook)
        {
            if (ModelState.IsValid)
            {
                db.StockBooks.Add(stockBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(stockBook);
        }

        // GET: StockBooks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockBook stockBook = db.StockBooks.Find(id);
            if (stockBook == null)
            {
                return HttpNotFound();
            }
            return View(stockBook);
        }

        // POST: StockBooks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,title,cost")] StockBook stockBook)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stockBook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(stockBook);
        }

        // GET: StockBooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockBook stockBook = db.StockBooks.Find(id);
            if (stockBook == null)
            {
                return HttpNotFound();
            }
            return View(stockBook);
        }

        // POST: StockBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StockBook stockBook = db.StockBooks.Find(id);
            db.StockBooks.Remove(stockBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
